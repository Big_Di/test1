import Vue from 'vue'
import Vuex from 'vuex';
import api from './components/Backend.js'
Vue.use(Vuex);



const store = new Vuex.Store({
    state: {
      badPassword: false,  
      isAutorizate: false,
      lockChanges: false,
      dataSources: {
          telegram: true,
          viber: false,
          facebook: false,
          widget: false,
          skype: false,
      },
      ds: {
        telegram: [],
        viber: [],
        facebook: [],
        widget: [],
        skype: [],
     },
      filter: {
          name: '',
          period: {
              from: 0,
              to: 0,
          }
      }

    },
    mutations: {
        lock(state) {
            state.lockChanges = true;
        },
        unlock(state) {
            state.lockChanges = false;
        },
        login (state) {
            state.isAutorizate = true;
            state.badPassword = false; 
        },
        logout(state) {
            state.isAutorizate = false;
        },
        showText(state) {
            state.badPassword = true;
        },
        setDataSourse(state, params) {
            state.dataSources[params.name] = params.value;
        },
        setFilter(state, newFilter) {
            state.filter = newFilter;
        },
        setDS(state, payload) {
            state.ds[payload.name] = payload.data.map(p => ({t: new Date(p.t * 1000), y: p.y}));
        }

    }, 
    actions: {
        checkSuccess ({commit}, {login, password}) {
            if(login === 'demo' && password === 'demo') {
                commit('login');
            } else {
                commit('logout');
                commit('showText');
            }
        },
        logout({commit}) {
            commit('logout');
        },
        setFilter({commit, state}, newFilter) {
            commit('lock');
            Promise.all(Object.keys(state.ds).map((key) => {
                if(!state.dataSources[key]) {
                    commit('setDS', {name: key,data: []});
                    return [];
                } 
                commit('setFilter', newFilter);
                return api.getData(key, state.filter.period).then((data) => {
                    commit('setDS', {name: key, data});
                    commit('unlock');
                })
            })).then(() => {
                commit('unlock');
            }).catch(()=> {
                commit('unlock');
            });
        },
        setDataSourse({commit, state}, params) {
            commit('setDataSourse', params);
            commit('lock');
            Promise.all(Object.keys(state.ds).map((key) => {
                if(!state.dataSources[key]) {
                    commit('setDS', {name: key,data: []});
                    return [];
                } 
                return api.getData(key, state.filter.period).then((data) => {
                    commit('setDS', {name: key, data});
                    commit('unlock');
                })
            })).then(() => {
                commit('unlock');
            }).catch(()=> {
                commit('unlock');
            });
        },    
    }
  });
window.store = store;
export default store;
