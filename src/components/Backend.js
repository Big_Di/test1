import moment from 'moment'; 


const hours = 8784; 
const m = moment();
const db = {
    telegram: [],
    viber: [],
    widget: [
        0.58,
        0.42,
    ],
    skype: [],
    facebook: [],
};

function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
}

function getData(name, period) {
    if(period && period.from < period.to) {
        throw new Error('bad period');
    }
    function filterCondition (point) {
        return point.t <= period.from && point.t >= period.to
    }
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(db[name].filter(filterCondition));
        });
    })
}

for(let i = 0; i < hours; i++ ){
    const t = m.unix();
    m.subtract(1, 'hours');
    db.telegram.push({
        t,
        y: randomInteger(0, 15),
    });
    db.viber.push({
        t,
        y: randomInteger(0, 10),
    });
    db.facebook.push({
        t,
        y: randomInteger(0, 20),
    })
}

export default {
    db,
    getData,
}