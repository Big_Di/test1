function getColor(colorName) {
    if(colorName === 'facebook') {
        return {
            borderColor: 'rgba(46, 146, 242, 1)',
            backgroundColor: 'rgba(46, 146, 242, .2)',
            pointBackgroundColor: 'rgba(46, 146, 242, 1)',
        }
    }

    if(colorName === 'viber') {
        return {
            borderColor: 'rgba(107, 125, 234, 1)',
            backgroundColor: 'rgba(107, 125, 234, .2)',
            pointBackgroundColor: 'rgba(107, 125, 234, 1)',
        }
    }
    if(colorName === 'telegram')
    return {
        borderColor: 'rgba(255, 64, 129, 1)',
        backgroundColor: 'rgba(255, 64, 129, .2)',
        pointBackgroundColor: 'rgba(255, 64, 129, 1)',
    }
}

export default {
    getColor
}