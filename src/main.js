import 'babel-polyfill';

import Vue from 'vue'
import './globals.css'
import VueRouter from 'vue-router'
import VuePikaday from '@enrian/vue-pikaday';
import '@enrian/vue-pikaday/dist/vue-pikaday.min.css';
import App from "./components/App.vue"
import MainPage from './components/MainPage.vue'
import Access from './components/Access.vue'
import SummaryData from './components/Data/SummaryData.vue'
import VisitorsData from './components/Data/VisitorsData.vue'
import SessionData from './components/Data/SessionData.vue'
import store from './Store'



Vue.use(VuePikaday);
Vue.use(VueRouter);


const router = new VueRouter({
  routes: [
    { path: '/login', component: Access },
    { path: '/home', component: MainPage, 
    children: [
      {
        path: 'summary',
        component: SummaryData
      },
      {
        path: 'visitors',
        component: VisitorsData
      },
      {
        path: 'session',
        component: SessionData
      },
      {
      path: '/home', redirect: '/home/summary'
    }
    ]
    },
  ]
})


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');




Vue.config.productionTip = false



